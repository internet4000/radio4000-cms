import './list-model.js'

const template = document.createElement('template')

template.innerHTML = `
	<style>
		:host([hidden]) { display: none }
		:host {
			display: flex;
		}
		.Component {
		}
	</style>
	<div class="Component"></div>
`

class ListItem extends HTMLElement {
	constructor() {
		super()
		this.attachShadow({
			mode: 'open'
		})
		this.shadowRoot.appendChild(template.content.cloneNode(true))
	}

	connectedCallback() {
		this.id = this.getAttribute('id') || ''
		this.title = this.getAttribute('title') || ''
		this.render()
	}

	render() {
		const $component = this.shadowRoot.querySelector('.Component')

		let $modelItem = document.createElement('p')
		$modelItem.innerText = this.id || this.title

		$component.append($modelItem)
	}
}

export default ListItem
