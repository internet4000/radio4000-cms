import {camelCase} from '../utils/index.js'

import {
	onModels
} from '../firebase/api/index.js'

import WithDatabase from '../firebase/with-database.js'

class ListModels extends WithDatabase {
	static get observedAttributes() {
		return ['model-type']
	}

	attributeChangedCallback(name, oldValue, newValue) {
		this[camelCase(name)] = newValue
	}

	connectedCallback(event) {
		this.modelType = this.modelType || this.getAttribute('model-type') || ''

		let limit = this.getAttribute('limit')
		let limitNumber
		try {
			limitNumber = Number(limit)
		} catch(error) {
			limitNumber = null
		}

		if (limitNumber && typeof limitNumber === 'number') {
			this.limit = limitNumber
		}

		super.connectedCallback(event)
	}

	databaseCallback = async (event) => {
		super.databaseCallback(event)

		let model
		try {
			model = await onModels(this.firebase, this.modelType)
		} catch (error) {
			console.log('Error getting model', error)
		}

		if (this.limit && typeof this.limit === 'number') {
			this.model = model && model.slice(0, this.limit)
		} else {
			this.model = model
		}

		this.render()
	}

	render() {
		const $component = this

		if (this.model.length) {
			this.model.forEach(item => {
				let jobUrl
				const {
					title
				} = item

				let $modelItem = document.createElement('list-item')

				title && $modelItem.setAttribute('title', title)
				$component.append($modelItem)
			})
		}
	}
}

export default ListModels
