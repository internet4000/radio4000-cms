import {camelCase} from '../utils/index.js'

import {
	onModel,
	onModelQuery
} from '../firebase/api/index.js'

class ListModel extends HTMLElement {
	static get observedAttributes() {
		return ['model-type', 'model-id', 'model-slug']
	}

	attributeChangedCallback(name, oldValue, newValue) {
		this[camelCase(name)] = newValue
	}

	beforeConnectedCallback = () => {}

	connectedCallback() {
		this.beforeConnectedCallback()
		this.modelType = this.getAttribute('model-type') || ''
		this.modelId = this.getAttribute('model-id') || ''
		this.modelSlug = this.getAttribute('model-slug') || ''

		if (window.firebase) {
			this.handleDatabaseReady({
				detail: window.firebase
			})
		}

		const $firebaseApp = document.querySelector('firebase-app')
		if ($firebaseApp) {
			$firebaseApp.addEventListener('firebaseReady', this.handleDatabaseReady)
		}
	}

	disconnectedCallback = () => {
		const $firebaseApp = document.querySelector('firebase-app')
		if ($firebaseApp) {
			$firebaseApp.removeEventListener('firebaseReady', this.handleDatabaseReady)
		}
	}

	handleDatabaseReady = async ({detail}) => {
		this.firebase = detail

		let model
		if (!this.modelId && this.modelSlug) {
			try {
				model = await onModelQuery(this.firebase, this.modelType, this.modelSlug)
			} catch (error) {
				console.log('Error querying model by slug', error)
			}
		} else {
			try {
				model = await onModel(this.firebase, this.modelType, this.modelId)
			} catch (error) {
				console.log('Error getting model', error)
			}
		}

		this.model = model

		this.render()
	}

	render() {
		if (!this.model) return

		const $component = this
		const {
			title
		} = this.model

		let $modelItem = document.createElement('list-item')
		title && $modelItem.setAttribute('title', title)
		$component.append($modelItem)
	}
}

export default ListModel
