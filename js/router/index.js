import page from 'https://cdn.skypack.dev/page'
import {sitePathname} from '../env.js'

// https://github.com/visionmedia/page.js/issues/537
page.configure({window: window})

const buildHref = (itemHref) => {
    let href
    if (sitePathname) {
	href = `${sitePathname}${itemHref}`
    } else {
	href = itemHref
    }
    return href
}

const buildLink = ({text, href, title}) => {
    const linkHref = buildHref(href)
    let $link = document.createElement('a')

    $link.setAttribute('href', linkHref)

    if (text) {
	$link.innerText = text
    } else if (title) {
	$link.setAttribute('title', title)
    }

    $link.onclick = event => {
	page(linkHref)
	event.preventDefault()
    }

    return $link
}

/* if we have a path-name, use it as path base */
if (sitePathname) {
    page.base(sitePathname)
}

export {
    page,
    buildHref,
    buildLink
}
