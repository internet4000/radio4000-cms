import WithDatabase from './with-database.js'

class WithAuth extends WithDatabase {
  connectedCallback() {
		super.connectedCallback()
		const $databaseComponent = document.querySelector(this.databaseDomSelector)

		if (this.databaseWindowSelector && window[this.databaseWindowSelector]) {
			this.userCallback({
				detail: window.firebase.auth().currentUser
			})
		}

		if ($databaseComponent) {
			$databaseComponent.addEventListener('userLoggedIn', this.userCallback, false)
		}
	}

	disconnectedCallback() {
		super.disconnectedCallback()
		const $databaseComponent = document.querySelector(this.databaseDomSelector)
		if ($databaseComponent) {
			$databaseComponent.removeEventListener('userLoggedIn', this.userCallback, false)
		}
	}

	userCallback(event) {
		if (event && event.detail) {
			this.user = event.detail
		}
	}
}

customElements.define('with-auth', WithAuth)

export default WithAuth
