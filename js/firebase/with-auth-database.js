import WithAuth from './with-auth.js'

class WithAuthDatabase extends WithAuth {
	userCallback = (event) => {
		super.userCallback()
		if (event && event.detail) {
			this.user = event.detail
		}
	}
}

customElements.define('with-auth-database', WithAuthDatabase)

export default WithAuthDatabase
