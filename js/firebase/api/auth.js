/* firebase auth, user things */
const loginUser = (firebaseInstance, email, password) => {
    return firebaseInstance
	.auth()
	.signInWithEmailAndPassword(email, password)
}

const logoutUser = (firebaseInstance) => firebaseInstance.auth().signOut()

const registerUser = (firebaseInstance, email, password) => {
    return firebaseInstance
	.auth()
	.createUserWithEmailAndPassword(email, password)
}

const deleteUser = (firebaseInstance) => {
    const user = firebaseInstance.auth().currentUser
    return user.delete()
}

const updateUserEmail = (firebaseInstance, email) => {
    return firebaseInstance
	.auth()
	.currentUser
	.updateEmail(email)
}

function sendPasswordResetEmail(firebaseInstance, email) {
    return firebaseInstance
	.auth()
	.sendPasswordResetEmail(email);
}

function sendVerificationEmail(firebaseInstance) {
    const user = firebaseInstance.auth().currentUser
    return user.sendEmailVerification()
}

export {
    updateUserEmail,
    deleteUser,
    registerUser,
    loginUser,
    logoutUser,
    sendPasswordResetEmail,
    sendVerificationEmail
}
