import {
	serializeList,
	serializeItem
} from './serializer.js'

/* info: modelType is plural */

/* firebase auth, user things */
const onModels = (firebaseInstance, modelType = '' ) => {
	if (!firebaseInstance || !modelType) return

	return new Promise((resolve, reject) => {
		const ref = firebaseInstance.database().ref(`/${modelType}`)
		ref.on('value', (dataSnapshot) => {
			const data = serializeList(dataSnapshot)
			resolve(data)
		})
	})
}

const onModel = (firebaseInstance, modelType = '', modelId = '') => {
	if (!firebaseInstance || !modelType) return

	return new Promise((resolve, reject) => {
		const ref = firebaseInstance
			.database().ref(`/${modelType}/${modelId}`)

		ref.on('value', (dataSnapshot) => {
			const data = serializeItem(dataSnapshot)
			resolve(data)
		})
	})
}

const onModelQuery = (firebaseInstance, modelType = '', modelSlug = '') => {
	if (!firebaseInstance || !modelType) return

	return new Promise((resolve, reject) => {
		const ref = firebaseInstance.database().ref(`/${modelType}`)
		ref.orderByChild('slug').equalTo(modelSlug).on('value', (dataSnapshot) => {
			const data = serializeList(dataSnapshot)[0]
			resolve(data)
		})
	})
}

const createModel = (firebaseInstance, modelType = '', modelData) => {
	const newModelRef = firebaseInstance.database().ref(`/${modelType}`)
	return newModelRef.push(modelData)
}
const deleteModel = (firebaseInstance, modelType = '', modelId)  => {
	const modelRef = firebaseInstance.database().ref(`/${modelType}/${modelId}`)
	return modelRef.remove()
}
const editModel = (firebaseInstance, modelType = '', modelId, modelData) => {
	const modelRef = firebaseInstance.database().ref(`/${modelType}/${modelId}`)
	return modelRef.update(modelData)
}

/* https://stackoverflow.com/questions/34718668/firebase-timestamp-to-date-and-time */
const getServerTime = async (firebaseInstance) => {
	return await new Promise((resolve, reject) => {
		try {
			firebaseInstance.database().ref("/.info/serverTimeOffset").on('value', function(offset) {
				var offsetVal = offset.val() || 0;
				var serverTime = Date.now() + offsetVal;
				resolve(serverTime)
			})
		} catch(error) {
			reject(error)
		}
	})
}

const serializeNewModel = (modelData) => {
	/* note the _ after pieces_ */
	return {
		id: modelData.path.pieces_[1]
	}
}

const createChannel = async (firebaseInstance, modelData) => {
	let serverTime
	try {
		serverTime = await getServerTime(firebaseInstance)
	} catch(error) {
		console.log('Cold not get time server', error)
		return
	}
	modelData.created = serverTime
	modelData.slug = modelData.title
	modelData.isFeatured = false
	console.log('model-data', modelData)

	let newModel
	try {
		newModel = await createModel(firebaseInstance, 'channels', modelData).then(data => {
			return serializeNewModel(data)
		})
	} catch (error) {
		console.log('Error creating new channel model', error)
		return
	}
	console.log('newModel', newModel)
	return newModel
}

export {
  onModels,
	onModel,
	onModelQuery,
	deleteModel,
	editModel,
	createChannel
}
