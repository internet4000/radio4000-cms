import FormModel from '../model/form.js'

class FormChannel extends FormModel {

	get formAttributes() {
		return ['title']
	}

	constructor() {
		super()
	}
}

customElements.define('form-channel', FormChannel)

export default FormChannel
