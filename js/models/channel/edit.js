import ModelEdit from '../model/edit.js'

class ChannelEdit extends ModelEdit {
	beforeConnectedCallback = () => {
		this.setAttribute('model-type', 'channels')
	}
}

customElements.define('channel-edit', ChannelEdit)

export default ChannelEdit
