import {
	onModel
} from '../../firebase/api/index.js'

import './form.js'
import './item.js'

class ModelCreate extends HTMLElement {
	beforeConnectedCallback = () => {}
	connectedCallback() {
		this.beforeConnectedCallback()
		this.modelType = this.getAttribute('model-type') || ''
		this.submitText = this.getAttribute('submit-text') || 'Submit'

		const $firebaseApp = document.querySelector('firebase-app')

		if (window.firebase) {
			this.handleDatabaseReady({
				detail: window.firebase
			})
			this.handleUserLoggedIn({
				detail: window.firebase.auth().currentUser
			})
		}

		if ($firebaseApp) {
			$firebaseApp.addEventListener('firebaseReady', this.handleDatabaseReady, false)
			$firebaseApp.addEventListener('userLoggedIn', this.handleUserLoggedIn, false)
		}
	}

	get formElement() {
		return 'form-model'
	}

	disconnectedCallback() {
		const $firebaseApp = document.querySelector('firebase-app')
		if ($firebaseApp) {
			$firebaseApp.removeEventListener('firebaseReady', this.handleDatabaseReady)
			$firebaseApp.removeEventListener('userLoggedIn', this.handleUserLoggedIn)
			/* this.removeEventListener('modelSubmitted', this.handleModelChanged) */
		}
	}
	handleDatabaseReady = ({detail}) => {
		this.firebase = detail
		this.render()
	}
	handleUserLoggedIn = ({detail}) => {
		this.user = detail
		this.render()
	}

	handleModelChanged = async ({detail}) => {
		const modelData = detail

		if (!modelData.title) {
			return
		}

		this.submitting = true

		let modelResponse
		try {
			modelResponse = await this.createModel(this.firebase, this.modelType, modelData)
		} catch (error) {
			console.log('Error creating model', error)
			this.submitting = false
			return
		}


		if (!modelResponse) {
			console.log('Error creating model')
			this.submitting = false
			return
		}

		let model
		try {
			model = await onModel(this.firebase, this.modelType, modelResponse.id)
		} catch (error) {
			console.log('Error getting model after successfull creation', error)
			this.submitting = false
			return
		}

		this.submitting = false
		this.model = model

		try {
			await this.afterCreateModel(this.model)
		} catch (error) {
			console.log('Error after model create', error)
		}

		this.render()
	}

	async afterCreateModel(model) {}

	async createModel(firebaseInstance, modelType, modelData) {
		console.log('Creating VOID model:', ...arguments)
	}

	render() {
		let $component = this
		$component.innerHTML = ''

		if (!this.user) {
			const $userMessage = document.createElement('p')
			$userMessage.innerText = `You need to be logged in to create new ${this.modelType || 'models'}.`
			$component.appendChild($userMessage)
			return
		}

		const $form = document.createElement(this.formElement)
		if (this.submitting) {
			form.setAttribute('submitting', true)
		}
		$form.setAttribute('submit-text', this.submitText || `New ${this.modelType || ''}`)
		$form.addEventListener('modelSubmitted', this.handleModelChanged, false)
		$component.appendChild($form)

		if (this.model) {
			const $newModelItem = document.createElement('model-item')
			$newModelItem.setAttribute('model-id', this.model.id)
			$newModelItem.setAttribute('title', this.model.title)
			$newModelItem.setAttribute('is-new', true)
			$component.appendChild($newModelItem)
		}
	}
}

/* here as example; how to define a custom-element web component */
customElements.define('model-create', ModelCreate)

export default ModelCreate
