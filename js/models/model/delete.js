import {camelCase} from '../../utils/index.js'
import {
	onModel,
	deleteModel
} from '../../firebase/api/index.js'

class ModelDelete extends HTMLElement {
	beforeConnectedCallback = () => {}
	connectedCallback() {
		this.beforeConnectedCallback()
		this.modelId = this.getAttribute('model-id')
		this.modelType = this.getAttribute('model-type')

		if (window.firebase) {
			this.handleDatabaseReady({
				detail: window.firebase
			})
			this.handleUserLoggedIn({
				detail: window.firebase.auth().currentUser
			})
		}

		const $firebaseApp = document.querySelector('firebase-app')
		if ($firebaseApp) {
			$firebaseApp.addEventListener('firebaseReady', this.handleDatabaseReady, false)
			$firebaseApp.addEventListener('userLoggedIn', this.handleUserLoggedIn, false)
		}
	}

	disconnectedCallback() {
		const $firebaseApp = document.querySelector('firebase-app')
		if ($firebaseApp) {
			$firebaseApp.removeEventListener('firebaseReady', this.handleDatabaseReady)
			$firebaseApp.removeEventListener('userLoggedIn', this.handleUserLoggedIn)
		}
	}
	attributeChangedCallback = (name, oldValue, newValue) => {
		this[camelCase(name)] = newValue
		this.render()
	}
	handleUserLoggedIn = ({detail}) => {
		this.user = detail
		this.render()
	}
	handleDatabaseReady = async ({detail}) => {
		this.firebase = detail

		let model
		try {
			model = await onModel(this.firebase, this.modelType, this.modelId)
		} catch (error) {
			console.log('Error getting model to delete', error)
		}

		this.model = model
		this.render()
	}

	handleClick = async (event) => {
		let deleted
		try {
			deleted = await deleteModel(this.firebase, this.modelType, this.modelId)
		} catch (error) {
			console.log('Error deleting model', this.modelType, this.modelId, error)
		}
		return deleted
	}

	render() {
		let $component = this
		$component.innerHTML = ''


		if (
			this.model
			&& Object.keys(this.model).length
		) {
			let $deleteButton = document.createElement('button')
			$deleteButton.innerText = 'Delete model'
			$deleteButton.onclick = this.handleClick
			$component.appendChild($deleteButton)
		}
	}
}

customElements.define('model-delete', ModelDelete)

export default ModelDelete
