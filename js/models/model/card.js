/*
	 params:
	 - model-id: a firebase id of a model
	 - model-type: ex: "channels"; the firebase root path of the model type
 */

import {camelCase} from '../../utils/index.js'
import {
	onModel
} from '../../firebase/api/index.js'

import './item.js'
import './edit.js'

class ModelCard extends HTMLElement {
	connectedCallback() {
		this.modelId = this.getAttribute('model-id')
		this.modelType = this.getAttribute('model-type')
		this.model = null

		if (window.firebase) {
			this.handleDatabaseReady({
				detail: window.firebase
			})
		}

		const $firebaseApp = document.querySelector('firebase-app')
		if ($firebaseApp) {
			$firebaseApp.addEventListener('firebaseReady', this.handleDatabaseReady, false)
			$firebaseApp.addEventListener('userLoggedIn', this.handleUserLoggedIn, false)
		}
	}

	disconnectedCallback() {
		const $firebaseApp = document.querySelector('firebase-app')
		if ($firebaseApp) {
			$firebaseApp.removeEventListener('firebaseReady', this.handleDatabaseReady)
			$firebaseApp.removeEventListener('userLoggedIn', this.handleUserLoggedIn)
		}
	}
	attributeChangedCallback = (name, oldValue, newValue) => {
		this[camelCase(name)] = newValue
		this.render()
	}
	handleDatabaseReady = async ({detail}) => {
		this.firebase = detail
		this.render()
	}
	handleUserLoggedIn = async ({detail}) => {
		if (!this.modelId) return

		this.user = detail
		try {
			await onModel(this.firebase, this.modelType, this.modelId, this.handleModel)
		} catch(error) {
			console.log(`Error fetching model ${this.modelType}/${this.modelId}`, error)
		}
	}
	handleModel = (model) => {
		this.model = model
		this.render()
	}

	render() {
		let $component = this

		$component.innerHTML = ''

		const modelExists = this.model && Object.keys(this.model).length
		if (!modelExists) {
			let $noModel = document.createElement('p')
			$noModel.innerHTML = `This model (${this.modelType}/${this.modelId}) does not seem to exist.`
			this.$component.appendChild($noModel)
			return
		}

		const {
			id,
			body,
			title
		} = this.model


		let $modelHeader = document.createElement('model-item')
		title && $modelHeader.setAttribute('title', title)
		id && $modelHeader.setAttribute('job-id', id)
		tags && tags.length && $modelHeader.setAttribute('tags', JSON.stringify(tags))

		let $modelBody = document.createElement('model-body')
		title && $modelBody.setAttribute('body', body)

		$component.append($modelHeader)
		$component.append($modelBody)
	}
}

/* here as eaxmple; how to define a custom-element web component */
customElements.define('model-card', ModelCard)

export default ModelCard
