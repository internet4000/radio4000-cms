import {page, buildLink} from './router.js'

class Radio4000ChannelCard extends HTMLElement {
	connectedCallback() {
		this.channelId = this.getAttribute('channel-id') || ''
		this.title = this.getAttribute('title') || ''
		this.slug = this.getAttribute('slug') || ''
		this.render()
	}

	render() {
		const $component = this

		let $modelItem = document.createElement('p')

		let $link = buildLink({
			href: `/${this.slug}`,
			title: this.title
		})

		let $button = document.createElement('button')
		$button.innerText = 'play'
		$button.onclick = () => {
			const $r4p = document.querySelector('radio4000-player')
			$r4p.channelId = this.channelId
		}
		$modelItem.append($link)
		$modelItem.append($button)

		$component.append($modelItem)
	}
}

customElements.define('radio4000-channel-card', Radio4000ChannelCard)

export default Radio4000ChannelCard
