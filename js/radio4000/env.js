const isDevelopment = window.location.origin.includes('localhost')
const sitePathname =  isDevelopment ? '' : '/radio4000-cms'

let firebaseConfig,
		algoliaConfig

if (isDevelopment) {
	firebaseConfig = {
		'api-key': 'AIzaSyA9B2FveOSmrW3Pf8ASPRLdSTd9X2Osz2Y',
		'auth-domain': 'radio4000-hugurp.firebaseapp.com',
		'database-url': 'https://radio4000-hugurp.firebaseio.com',
		'project-id': 'radio4000-hugurp',
		'storage-bucket': 'radio4000-hugurp.appspot.com',
		'messaging-sender-id': '999501188522',
		'app-id': '1:999501188522:web:bd2371419386e6590defd3',
		'firebase-auth': true,
		'firebase-realtime': true
	}
	// todo: add dev environment keys (now using prod)
	algoliaConfig = {
		'application-id': '7FFSURJR0X',
		'api-key': 'dba2e03ef95f278d5fbe76d4cd80b6bf'
	}
} else {
	firebaseConfig = {
    'api-key': "AIzaSyA6SVd2hS197y4LmWJoTcv4FZJ9y47JWqs",
    'auth-domain': "radio4000.firebaseapp.com",
    'database-url': "https://radio4000.firebaseio.com",
    'project-id': "firebase-radio4000",
    'storage-bucket': "firebase-radio4000.appspot.com",
    'messaging-sender-id': "561286102640",
    'app-id': "1:561286102640:web:06b167c1231dc1729afeb5",
		'firebase-auth': true,
		'firebase-realtime': true
  }
	algoliaConfig = {
		'application-id': '7FFSURJR0X',
		'api-key': 'dba2e03ef95f278d5fbe76d4cd80b6bf'
	}
}

const componentFromConfig = (componentName, componentConfig) => {
	let $component = document.createElement(componentName)
	if ($component) {
		Object.keys(componentConfig).forEach(configKey => {
			$component.setAttribute(configKey, componentConfig[configKey])
		})
	}
	return $component
}

export {
	isDevelopment,
	sitePathname,
	componentFromConfig,
	firebaseConfig,
	algoliaConfig
}
