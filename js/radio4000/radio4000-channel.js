import ListModel from '../lists/list-model.js'
import './radio4000-channel-card.js'
import '../models/channel/edit.js'
import '../models/channel/delete.js'

class Radio4000Channel extends ListModel {
	beforeConnectedCallback = () => {
		this.setAttribute('model-type', 'channels')
	}
	render() {
		if (!this.model) return

		const $component = this
		const {
			title,
			slug,
			id
		} = this.model

		let $channelCard = document.createElement('radio4000-channel-card')
		title && $channelCard.setAttribute('title', title)
		title && $channelCard.setAttribute('slug', slug)

		let $channelEdit = document.createElement('channel-edit')
		id && $channelEdit.setAttribute('model-id', id)

		let $channelDelete = document.createElement('channel-delete')
		id && $channelDelete.setAttribute('model-id', id)

		$component.append($channelCard)
		$component.append($channelEdit)
		$component.append($channelDelete)
	}
}

customElements.define('radio4000-channel', Radio4000Channel)

export default Radio4000Channel
