import ListModels from '../lists/list-models.js'
import './radio4000-channel-card.js'

class Radio4000Channels extends ListModels {
	constructor() {
		super()
		this.modelType = 'channels'
	}

	render() {
		let $component = this

		if (this.model && this.model.length) {
			this.model.forEach(item => {
				const {
					id,
					title,
					slug
				} = item

				let $channelCard = document.createElement('radio4000-channel-card')
				title && $channelCard.setAttribute('title', title)
				slug && $channelCard.setAttribute('slug', slug)
				id && $channelCard.setAttribute('channel-id', id)

				$component.append($channelCard)
			})
		}
	}
}

customElements.define('radio4000-channels', Radio4000Channels)

export default Radio4000Channels
