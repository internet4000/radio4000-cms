import {page} from '../router/index.js'
import {sitePathname} from './env.js'


const buildHref = (itemHref) => {
	let href
	if (sitePathname) {
		href = `${sitePathname}${itemHref}`
	} else {
		href = itemHref
	}
	return href
}

const buildLink = ({title, href}) => {
	const linkHref = buildHref(href)
	let $link = document.createElement('a')
	$link.innerText = title
	$link.href = linkHref
	$link.onclick = event => {
		page(linkHref)
		event.preventDefault()
	}
	return $link
}

// https://github.com/visionmedia/page.js/issues/537
page.configure({window: window})

/* if we have a path-name, use it as path base */
if (sitePathname) {
	page.base(sitePathname)
}

export {
	page,
	buildHref,
	buildLink
}
