import '//cdn.jsdelivr.net/npm/radio4000-player'

class Radio4000PlayerContainer extends HTMLElement {
	constructor() {
		super()
	}
	connectedCallback() {
		this.render()
	}

	handleDeck = event => {
		this.setAttribute('size', 'deck')
	}

	handleSmall = event => {
		this.setAttribute('size', 'small')
	}

	render() {
		const $player = document.createElement('radio4000-player')
		$player.channelSlug = 'ko002'

		const $buttonGroup = document.createElement('button-group')

		const $buttonDeck = document.createElement('button')
		$buttonDeck.innerText = 'D'
		$buttonDeck.title = '[D]eck'
		$buttonDeck.onclick = this.handleDeck

		const $buttonSmall = document.createElement('button')
		$buttonSmall.innerText = 'S'
		$buttonSmall.title = '[S]mall'
		$buttonSmall.onclick = this.handleSmall

		$buttonGroup.append($buttonDeck)
		$buttonGroup.append($buttonSmall)
		this.append($buttonGroup)
		this.append($player)
	}
}

customElements.define('radio4000-player-container', Radio4000PlayerContainer)

export default Radio4000PlayerContainer
